#ifndef OAK_KERNEL_ARCH_X86_LOGGING_H
#define OAK_KERNEL_ARCH_X86_LOGGING_H

void oak_debug(const char *format, ...);
void info(const char *format, ...);
void oak_warn(const char *format, ...);
void oak_error(const char *format, ...);
void oak_fatal(const char *format, ...);

#endif
