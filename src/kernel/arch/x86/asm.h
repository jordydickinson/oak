#ifndef OAK_KERNEL_ARCH_X86_ASM_H
#define OAK_KERNEL_ARCH_X86_ASM_H

#define cli() __asm__ __volatile__ ("cli");
#define sti() __asm__ __volatile__ ("sti");
#define hlt() __asm__ __volatile__ ("hlt");

#define invlpg(LOC)         \
    __asm__ __volatile__ (  \
        "invlpg %0"         \
        :                   \
        : "m" (LOC)         \
    )

#define rdmsr(MSR, DSTLO, DSTHI)        \
    __asm__ __volatile__ (              \
        "rdmsr"                         \
        : "=a" (DSTLO)                  \
        , "=d" (DSTHI)                  \
        : "c" (MSR)                     \
    )
#define wrmsr(MSR, SRCLO, SRCHI)        \
    __asm__ __volatile__ (              \
        "wrmsr"                         \
        :                               \
        : "a" (SRCLO)                   \
        , "d" (SRCHI)                   \
        , "c" (MSR)                     \
    )

#define rdcr0(DST)          \
    __asm__ __volatile__ (  \
        "mov %%cr0, %0"     \
        : "=r" (DST)        \
    )
#define wrcr0(SRC)          \
    __asm__ __volatile__ (  \
        "mov %0, %%cr0"     \
        :                   \
        : "r" (SRC)         \
    )
#define rdcr3(DST)       \
    __asm__ __volatile__ (  \
        "mov %%cr3, %0"     \
        : "=r" (DST)        \
    )
#define wrcr3(SRC)       \
    __asm__ __volatile__ (  \
        "mov %0, %%cr3"     \
        :                   \
        : "r" (SRC)         \
    )
#define wrcs(IMM)        \
    __asm__ __volatile__ (  \
        "jmp %0, $Lwcs\n"   \
        "Lwcs:"             \
        :                   \
        : "i" (IMM)         \
    )
#define wrds(SRC)        \
    __asm__ __volatile__ (  \
        "mov %%ax, %%ds"    \
        :                   \
        : "a" (SRC)         \
    )
#define wres(SRC)        \
    __asm__ __volatile__ (  \
        "mov %%ax, %%es"    \
        :                   \
        : "a" (SRC)         \
    )
#define wrfs(SRC)        \
    __asm__ __volatile__ (  \
        "mov %%ax, %%fs"    \
        :                   \
        : "a" (SRC)         \
    )
#define wrgs(SRC)        \
    __asm__ __volatile__ (  \
        "mov %%ax, %%gs"    \
        :                   \
        : "a" (SRC)         \
    )
#define wrss(SRC)        \
    __asm__ __volatile__ (  \
        "mov %%ax, %%ss"    \
        :                   \
        : "a" (SRC)         \
    )

#define lgdt(GDTR)       \
    __asm__ __volatile__ (  \
        "lgdt %0"           \
        :                   \
        : "m" (GDTR)        \
    )
#define lidt(IDTR)       \
    __asm__ __volatile__ (  \
        "lidt %0"           \
        :                   \
        : "m" (IDTR)        \
    )
#define ltr(TSS)         \
    __asm__ __volatile__ (  \
        "ltr %%ax"          \
        :                   \
        : "a" (TSS)         \
    )

#define inb(PORT, OUT)   \
    __asm__ __volatile__ (  \
        "inb %%dx, %%al"    \
        : "=a" (OUT)        \
        : "d" (PORT)        \
    )
#define inw(PORT, OUT)   \
    __asm__ __volatile__ (  \
        "inw %%dx, %%ax"    \
        : "=a" (OUT)        \
        : "d" (PORT)        \
    )
#define inl(PORT, OUT)   \
    __asm__ __volatile__ (  \
        "inl %%dx, %%eax"   \
        : "=a" (OUT)        \
        : "d" (PORT)        \
    )
#define outb(VAL, PORT)  \
    __asm__ __volatile__ (  \
        "outb %%al, %%dx"   \
        :                   \
        : "a" (VAL)         \
        , "d" (PORT)        \
    )
#define outw(VAL, PORT)  \
    __asm__ __volatile__ (  \
        "outw %%ax, %%dx"   \
        :                   \
        : "a" (VAL)         \
        , "d" (PORT)        \
    )
#define outl(VAL, PORT)  \
    __asm__ __volatile__ (  \
        "outl %%eax, %%dx"  \
        :                   \
        : "a" (VAL)         \
        , "d" (PORT)        \
    )

#endif
