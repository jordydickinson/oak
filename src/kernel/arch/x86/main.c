#include "asm.h"
#include "cpu/apic.h"
#include "cpu/gdt.h"
#include "cpu/idt.h"
#include "cpu/paging.h"
#include "cpu/pic.h"
#include "cpu/pit.h"
#include "cpu/tss.h"
#include "drivers/vga.h"
#include "logging.h"

void oak_main(void)
{
    oak_vga_init();
    oak_debug("VGA initialized");
    oak_pic_init();
    oak_apic_init();
    oak_debug("APIC initialized");
    oak_paging_init();
    oak_debug("Paging initialized");
    oak_tss_init();
    oak_debug("TSS initialized");

    cli();
    oak_gdt_init();
    oak_debug("GDT initialized");
    oak_idt_init();
    oak_debug("IDT initialized");
    oak_tss_flush();
    oak_debug("TSS flushed");
    oak_pit_init();
    oak_debug("PIT initialized");
    sti();

    while (1) { hlt(); }
}
