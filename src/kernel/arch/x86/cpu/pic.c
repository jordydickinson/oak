#include "pic.h"
#include "../asm.h"

enum {
    OAK_PORT_PIC1_CMD = 0x20,
    OAK_PORT_PIC1_DATA = 0x21,
    OAK_PORT_PIC2_CMD = 0xA0,
    OAK_PORT_PIC2_DATA = 0xA1,

    OAK_PIC_CMD_INIT = 0x10,
};

void oak_pic_init(void)
{
    // Remap the IRQs to the range 0x20-0x30.
    outb(OAK_PIC_CMD_INIT, OAK_PORT_PIC1_CMD);
    outb(OAK_PIC_CMD_INIT, OAK_PORT_PIC2_CMD);
    outb(0x20, OAK_PORT_PIC1_DATA);
    outb(0x28, OAK_PORT_PIC2_DATA);
    outb(0x04, OAK_PORT_PIC1_DATA);
    outb(0x02, OAK_PORT_PIC2_DATA);

    // Mask all interrupts.
    outb(0xFF, OAK_PORT_PIC1_DATA);
    outb(0xFF, OAK_PORT_PIC2_DATA);
}

void oak_pic_mask(size_t irq)
{
    if (irq < 8) {
        uint8_t mask;
        inb(OAK_PORT_PIC1_DATA, mask);
        mask |= 1 << irq;
        outb(mask, OAK_PORT_PIC1_DATA);
    } else if (irq < 16) {
        uint8_t mask;
        inb(OAK_PORT_PIC2_DATA, mask);
        mask |= 1 << (irq - 8);
        outb(mask, OAK_PORT_PIC2_DATA);
    } else {
        // oak_warn("PIC: Unable to mask IRQ: Invalid IRQ");
    }
}

void oak_pic_unmask(size_t irq)
{
    if (irq < 8) {
        uint8_t mask;
        inb(OAK_PORT_PIC1_DATA, mask);
        mask &= ~(1 << irq);
        outb(mask, OAK_PORT_PIC1_DATA);
    } else if (irq < 16) {
        uint8_t mask;
        inb(OAK_PORT_PIC2_DATA, mask);
        mask &= ~(1 << (irq - 8));
        outb(mask, OAK_PORT_PIC2_DATA);
    } else {
        // oak_warn("PIC: Unable to unmask IRQ: Invalid IRQ");
    }
}
