#include "pit.h"
#include "../asm.h"
#include "idt.h"

void oak_irq0(void);

void oak_pit_init(void)
{
    outb(0x34, 0x43);
    outb(0xFF, 0x40);
    outb(0xFF, 0x40);
    oak_idt_register(0x20, &oak_irq0, 0x08, 0x8E);
}
