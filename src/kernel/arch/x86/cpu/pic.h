#ifndef OAK_KERNEL_ARCH_X86_CPU_PIC_H
#define OAK_KERNEL_ARCH_X86_CPU_PIC_H

#include <stddef.h>
#include <stdint.h>

void oak_pic_init(void);
void oak_pic_mask(size_t irq);
void oak_pic_unmask(size_t irq);

#endif
