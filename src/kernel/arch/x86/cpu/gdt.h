#ifndef OAK_KERNEL_ARCH_X86_CPU_GDT_H
#define OAK_KERNEL_ARCH_X86_CPU_GDT_H

#include <stdbool.h>
#include <stdint.h>

typedef struct oak_descriptor {
    uint16_t limit_low  : 16;
    uint32_t base_low   : 24;
    uint8_t access      : 8;
    uint8_t limit_high  : 4;
    uint8_t flags       : 4;
    uint8_t base_high   : 8;
} __attribute__((packed)) oak_descriptor;

oak_descriptor oak_gdt[6];

void oak_gdt_init(void);

#endif
