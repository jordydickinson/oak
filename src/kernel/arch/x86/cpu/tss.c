#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "../asm.h"
#include "gdt.h"

typedef struct oak_tss_entry {
    uint32_t      : 32;
    uint32_t esp0 : 32;
    uint16_t      : 16;     uint16_t ss0 : 16;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
    uint32_t      : 32;
} __attribute__((packed)) oak_tss_entry;

extern uint8_t oak_stack[];
static oak_tss_entry oak_tss = {0};

void oak_tss_init(void)
{
    oak_gdt[5] = (oak_descriptor) {
        .limit_low = sizeof(oak_tss_entry),
        .base_low = (uint32_t)&oak_tss,
        .access = 0xE9,
        .limit_high = sizeof(oak_tss_entry) >> 16,
        .flags = 0,
        .base_high = (uint32_t)&oak_tss >> 24,
    };

    oak_tss.ss0 = 0x08;
    oak_tss.esp0 = (uint32_t)oak_stack;
}

void oak_tss_flush(void)
{
    ltr(0x2B);
}

void oak_tss_set_esp0(uint32_t esp0)
{
    oak_tss.esp0 = esp0;
}
