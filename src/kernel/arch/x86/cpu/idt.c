#include "idt.h"
#include "../asm.h"
#include "exceptions.h"

typedef struct oak_idt_entry {
    uint16_t offset_low;
    uint16_t selector;
    uint8_t zero;
    uint8_t type_attr;
    uint16_t offset_high;
} __attribute__((packed)) oak_idt_entry;

oak_idt_entry oak_idt[256] __attribute__ ((aligned(4)));

struct {
    uint16_t limit;
    uint32_t base;
} oak_idtr;

void oak_idt_register(size_t i, oak_isr isr, uint16_t selector, uint8_t type_attr);

void oak_idt_init(void)
{
    oak_idt_register(0xD, &oak_gpf, 0x08, 0x8E);
    oak_idt_register(0xE, &oak_pf, 0x08, 0x8E);
    oak_idtr.limit = sizeof(oak_idt) - 1;
    oak_idtr.base = (uint32_t)oak_idt;
    lidt(oak_idtr);
}

void oak_idt_register(size_t i, oak_isr isr, uint16_t selector, uint8_t type_attr)
{
    oak_idt[i].offset_low = (uint32_t)isr;
    oak_idt[i].offset_high = (uint32_t)isr >> 16;
    oak_idt[i].selector = selector;
    oak_idt[i].type_attr = type_attr;
    oak_idt[i].zero = 0;
}
