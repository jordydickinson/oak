#include "gdt.h"
#include "../asm.h"

oak_descriptor oak_gdt[6] = {
    // Null
    {0},
    // Kernel CS
    {
        .limit_low = 0xFFFF,
        .base_low = 0x000000,
        .access = 0x9A,
        .limit_high = 0xF,
        .flags = 0xC,
        .base_high = 0x00,
    },
    // Kernel DS
    {
        .limit_low = 0xFFFF,
        .base_low = 0x000000,
        .access = 0x92,
        .limit_high = 0xF,
        .flags = 0xC,
        .base_high = 0x00,
    },
    // User CS
    {
        .limit_low = 0xFFFF,
        .base_low = 0x000000,
        .access = 0xFA,
        .limit_high = 0xF,
        .flags = 0xC,
        .base_high = 0x00,
    },
    // User DS
    {
        .limit_low = 0xFFFF,
        .base_low = 0x000000,
        .access = 0xF2,
        .limit_high = 0xF,
        .flags = 0xC,
        .base_high = 0x00,
    },
    // TSS (to be filled in at runtime)
    {0},
};

static struct {
    uint16_t limit;
    uint32_t base;
} __attribute__ ((packed)) oak_gdtr;

void oak_gdt_init(void)
{
    oak_gdtr.limit = sizeof(oak_gdt);
    oak_gdtr.base = (uint32_t)oak_gdt;
    lgdt(oak_gdtr);
    wrcs(0x08);
    wrds(0x10);
    wres(0x10);
    wrfs(0x10);
    wrgs(0x10);
    wrss(0x10);
}
