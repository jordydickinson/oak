#include <stddef.h>
#include <stdint.h>
#include "../asm.h"

uint32_t oak_pdt[1024] __attribute__ ((aligned(4096)));
uint32_t *const oak_pts = (uint32_t*)0x00400000;
uint32_t *const oak_pfs_bottom = (uint32_t*)0x00800000;
uint32_t *oak_pfs_top = (uint32_t*)0x00800000;
const size_t OAK_PAGE_SIZE = 4096;
void *oak_break = (void*)0x00C00000;
void *oak_watermark = (void*)0x00C00000;

void oak_paging_init(void)
{
    // Register all page tables with the PDT.
    for (size_t i = 0; i < 1024; i++) {
        oak_pdt[i] = (uint32_t)(oak_pts + i * 1024);
        oak_pdt[i] |= 7;
    }

    // Identity-map the first 12 MiB.
    // Our kernel is loaded at 0x00100000.
    // The page tables are located at 0x00400000 until 0x00800000.
    // The page frame stack is located at 0x00800000 until 0x00C00000.
    for (size_t i = 0; i < 3072; i++) {
        oak_pts[i] = i << 12;
        oak_pts[i] |= 3;
    }

    // Push all free page frames onto `oak_pfs`.
    for (size_t i = 1048576; i >= 3072; i--) {
        *oak_pfs_top = i << 12;
        oak_pfs_top++;
    }

    // Indicate the location of the PDT.
    wrcr3(oak_pdt);
    // Activate paging.
    uint32_t cr0;
    rdcr0(cr0);
    cr0 |= 0x80000000;
    wrcr0(cr0);
}
