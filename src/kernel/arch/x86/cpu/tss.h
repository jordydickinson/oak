#ifndef OAK_KERNEL_ARCH_X86_CPU_TSS_H
#define OAK_KERNEL_ARCH_X86_CPU_TSS_H

void oak_tss_init(void);
void oak_tss_flush(void);

#endif
