#ifndef OAK_KERNEL_ARCH_X86_CPU_IDT_H
#define OAK_KERNEL_ARCH_X86_CPU_IDT_H

#include <stddef.h>
#include <stdint.h>

typedef void (*oak_isr)();

void oak_idt_init(void);
void oak_idt_register(size_t i, oak_isr isr, uint16_t selector, uint8_t type_attr);

#endif
