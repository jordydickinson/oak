#ifndef OAK_KERNEL_ARCH_X86_CPU_EXCEPTIONS_H
#define OAK_KERNEL_ARCH_X86_CPU_EXCEPTIONS_H

void oak_gpf(void);
void oak_pf(void);

#endif
