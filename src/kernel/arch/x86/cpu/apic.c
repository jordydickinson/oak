#include "apic.h"
#include <stdint.h>
#include "../asm.h"

#define OAK_APIC_BASE_MSR 0x1B
#define OAK_APIC_BASE_MSR_ENABLE 0x800

__attribute__((aligned(16)))
static uint32_t oak_apic_registers[256] __attribute__((aligned(4096)));

void oak_apic_init(void)
{
    // Set the base address of the APIC registers and enable the APIC.
    uint32_t lo = (uint32_t)oak_apic_registers | OAK_APIC_BASE_MSR_ENABLE;
    wrmsr(OAK_APIC_BASE_MSR, lo, 0);

    // Set the Spurious Interrupt Vector Register bit 8 to start receiving
    // interrupts.
    oak_apic_registers[0xF] = oak_apic_registers[0xF] | 0x100;
}
