#include <stdlib.h>
#include "logging.h"
#include "asm.h"

_Noreturn void abort(void)
{
    oak_fatal("Kernel panic");
    while (1) { hlt(); }
}
