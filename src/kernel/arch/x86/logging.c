#include "logging.h"
#include <ctype.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "drivers/vga.h"

static void oak_log_putc(char c)
{
    static size_t oak_log_x = 0, oak_log_y = 0;

    if (c == '\n') {
        oak_log_x = 0;
        oak_log_y++;
        return;
    }

    if (oak_log_x >= OAK_VGA_WIDTH) {
        oak_log_x = 0;
        oak_log_y++;
    }

    // TODO: Scrolling

    oak_vga_putc(c, oak_log_x, oak_log_y, OAK_VGA_COLOR_LIGHT_GRAY, OAK_VGA_COLOR_BLACK);
    oak_log_x++;
}

static void oak_logv(const char *prefix, const char *format, va_list ap)
{
    size_t prefix_len = strlen(prefix);
    size_t format_len = strlen(format);

    for (size_t i = 0; i < prefix_len; i++) {
        oak_log_putc(prefix[i]);
    }

    for (size_t i = 0; i < format_len; i++) {
        if (format[i] == '%') {
            i++;

            bool has_prefix = false;
            if (format[i] == '#') {
                has_prefix = true;
                i++;
            }

            size_t field_width = 0;
            while (isdigit(format[i])) {
                field_width *= 10;
                field_width += format[i] - '0';
                i++;
            }

            int size_modifier = 0;
            if (format[i] == 'h') {
                size_modifier--;
                i++;
            }

            if (format[i] == 'x' || format[i] == 'X') {
                char hex_a = format[i] == 'x' ? 'a' : 'A';
                unsigned long long arg;

                switch (size_modifier) {
                    case -2:
                    case -1:
                    case 0:
                    default:
                    arg = va_arg(ap, unsigned int);
                    break;

                    case 1:
                    arg = va_arg(ap, unsigned long);
                    break;

                    case 2:
                    arg = va_arg(ap, unsigned long long);
                    break;
                }

                if (has_prefix) {
                    oak_log_putc('0');
                    oak_log_putc('x');
                }

                for (size_t j = field_width; j > 0; j--) {
                    uint8_t nibble = arg >> (4 * (j - 1));
                    nibble &= 0xF;
                    if (nibble < 0xA) {
                        oak_log_putc('0' + nibble);
                    } else {
                        oak_log_putc(hex_a + nibble - 0xA);
                    }
                }
            }
        } else {
            oak_log_putc(format[i]);
        }
    }

    oak_log_putc('\n');
}

void oak_debug(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    oak_logv("[DEBUG]: ", format, ap);
    va_end(ap);
}

void info(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    oak_logv("[INFO]: ", format, ap);
    va_end(ap);
}

void oak_warn(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    oak_logv("[WARN]: ", format, ap);
    va_end(ap);
}

void oak_error(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    oak_logv("[ERROR]: ", format, ap);
    va_end(ap);
}

void oak_fatal(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    oak_logv("[FATAL]: ", format, ap);
    va_end(ap);
}
