#ifndef OAK_KERNEL_ARCH_X86_DRIVERS_VGA_H
#define OAK_KERNEL_ARCH_X86_DRIVERS_VGA_H

#include <stddef.h>

typedef enum oak_vga_color {
    OAK_VGA_COLOR_BLACK = 0,
    OAK_VGA_COLOR_BLUE = 1,
    OAK_VGA_COLOR_GREEN = 2,
    OAK_VGA_COLOR_CYAN = 3,
    OAK_VGA_COLOR_RED = 4,
    OAK_VGA_COLOR_MAGENTA = 5,
    OAK_VGA_COLOR_BROWN = 6,
    OAK_VGA_COLOR_LIGHT_GRAY = 7,
    OAK_VGA_COLOR_DARK_GRAY = 8,
    OAK_VGA_COLOR_LIGHT_BLUE = 9,
    OAK_VGA_COLOR_LIGHT_GREEN = 10,
    OAK_VGA_COLOR_LIGHT_CYAN = 11,
    OAK_VGA_COLOR_LIGHT_RED = 12,
    OAK_VGA_COLOR_LIGHT_MAGENTA = 13,
    OAK_VGA_COLOR_LIGHT_BROWN = 14,
    OAK_VGA_COLOR_LIGHT_WHITE = 15,
} oak_vga_color;

const size_t OAK_VGA_WIDTH;
const size_t OAK_VGA_HEIGHT;

void oak_vga_init(void);
void oak_vga_putc(char c, size_t x, size_t y, oak_vga_color fg, oak_vga_color bg);

#endif
