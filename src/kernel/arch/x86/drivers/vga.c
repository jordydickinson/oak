#include "vga.h"
#include <stdint.h>

const size_t OAK_VGA_WIDTH = 80;
const size_t OAK_VGA_HEIGHT = 25;
uint16_t *oak_vga_buf = (uint16_t*)0x000B8000;

void oak_vga_init(void)
{
    for (size_t x = 0; x < OAK_VGA_WIDTH; x++) {
        for (size_t y = 0; y < OAK_VGA_HEIGHT; y++) {
            oak_vga_putc(' ', x, y, OAK_VGA_COLOR_LIGHT_GRAY, OAK_VGA_COLOR_BLACK);
        }
    }
}

void oak_vga_putc(char c, size_t x, size_t y, oak_vga_color fg, oak_vga_color bg)
{
    if (x >= OAK_VGA_WIDTH) return;
    if (y >= OAK_VGA_HEIGHT) return;
    size_t i = x + y * OAK_VGA_WIDTH;
    oak_vga_buf[i] = (uint16_t)c | (uint16_t)fg << 8 | (uint16_t)bg << 12;
}
