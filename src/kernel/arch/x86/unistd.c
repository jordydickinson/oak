#include <unistd.h>
#include <errno.h>
#include <stddef.h>
#include <string.h>
#include "asm.h"

extern uint32_t *oak_pts;
extern uint32_t *const oak_pfs_bottom;
extern uint32_t *oak_pfs_top;
extern const size_t OAK_PAGE_SIZE;
extern void *oak_break;
extern void *oak_watermark;

int brk(void *addr)
{
    while (addr >= oak_break) {
        if (oak_pfs_top == oak_pfs_bottom) {
            errno = ENOMEM;
            return -1;
        }

        size_t pts_i = (uint32_t)oak_break >> 12;
        oak_pfs_top--;
        oak_pts[pts_i] = *oak_pfs_top | 3;
        memset(oak_break, 0, OAK_PAGE_SIZE);
        oak_break = (uint8_t*)oak_break + OAK_PAGE_SIZE;
    }

    while ((uint8_t*)addr < (uint8_t*)oak_break - OAK_PAGE_SIZE) {
        oak_break = (uint8_t*)oak_break + OAK_PAGE_SIZE;
        size_t pts_i = (uint32_t)oak_break >> 12;
        *oak_pfs_top = (uint32_t)oak_pts[pts_i];
        oak_pfs_top++;
        oak_pts[pts_i] = 0;
    }

    return 0;
}

void* sbrk(intptr_t incr)
{
    void *ret = oak_watermark;
    oak_watermark = (uint8_t*)oak_watermark + incr;
    brk(oak_watermark);
    return ret;
}
