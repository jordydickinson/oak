#include <string.h>

void* memchr(const void *buf, int c, size_t n)
{
    const char *dst_s = buf;

    for (size_t i = 0; i < n; i++) {
        if (dst_s[i] == c) {
            return (void*)(dst_s + i); // NOTE: Discarding `const` qualifier.
        }
    }

    return NULL;
}

int memcmp(const void *lhs, const void *rhs, size_t n)
{
    const char *lhs_s = lhs;
    const char *rhs_s = rhs;

    for (size_t i = 0; i < n; i++) {
        if (lhs_s[i] < rhs_s[i]) {
            return -1;
        }

        if (lhs_s[i] > rhs_s[i]) {
            return 1;
        }
    }

    return 0;
}

void* memcpy(void *dst, void *src, size_t n)
{
    char *dst_s = dst;
    char *src_s = src;

    for (size_t i = 0; i < n; i++) {
        dst_s[i] = src_s[i];
    }

    return dst;
}

void* memset(void *dst, int c, size_t n)
{
    char *dst_s = dst;

    for (size_t i = 0; i < n; i++) {
        dst_s[i] = c;
    }

    return dst;
}

size_t strlen(const char *str)
{
    size_t len = 0;

    while (str[len]) {
        len++;
    }

    return len;
}
