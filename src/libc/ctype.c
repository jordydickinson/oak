#include <ctype.h>

int isalnum(int c)
{
    return isalpha(c) || isdigit(c);
}

int isalpha(int c)
{
    return isupper(c) || islower(c);
}

int islower(int c)
{
    return c >= 'a' && c <= 'z';
}

int isupper(int c)
{
    return c >= 'A' && c <= 'Z';
}

int isdigit(int c)
{
    return c >= '0' && c <= '9';
}

int isxdigit(int c)
{
    return isdigit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

int iscntrl(int c)
{
    return (c >= 0 && c <= 0x1F) || c == 0x7F;
}

int isgraph(int c)
{
    return isalnum(c) || ispunct(c);
}

int isspace(int c)
{
    return c == ' ' || (c >= 0x09 && c <= 0x0D);
}

int isblank(int c)
{
    return c == ' ' || c == '\t';
}

int isprint(int c)
{
    return isgraph(c) || c == ' ';
}

int ispunct(int c)
{
    return (c >= 0x21 && c <= 0x2F)
        || (c >= 0x3A && c <= 0x40)
        || (c >= 0x5B && c <= 0x60)
        || (c >= 0x7B && c <= 0x7E);
}

int tolower(int c)
{
    if (isupper(c)) {
        return c - 'A' + 'a';
    }

    return c;
}

int toupper(int c)
{
    if (islower(c)) {
        return c - 'a' + 'A';
    }

    return c;
}
