Oak is an operating system, currently targeting x86 CPUs. However, plans are to
eventually target ARM systems such as the Raspberry Pi, and possibly RISC-V
systems if and when those become available.

# Build Prerequisites

You'll need [tup](http://gittup.org/tup/) and a cross compiler toolchain for
your target architecture. As of right now, the only target architecture
supported is i686. The following is an example of how to get a cross compiler
working for i686:

```sh
# Download binutils and gcc
wget http://ftpmirror.gnu.org/binutils/binutils-2.29.tar.xz http://ftpmirror.gnu.org/gcc/gcc-7.2.0/gcc-7.2.0.tar.xz

# Extract them
tar xf binutils-2.29.tar.xz &
tar xf gcc-7.2.0.tar.xz &
wait

# binutils and gcc require out-of-source builds, so let's create directories
mkdir -p build/binutils build/gcc

# Now we build/install binutils
pushd build/binutils
../../binutils-2.29/configure --target=i686-elf
make -j4
sudo make install
popd

# And now GCC
pushd build/gcc
../../gcc-7.2.0/configure --target=i686-elf --without-headers --enable-languages=c
make -j4 all-gcc all-target-libgcc
sudo make install-gcc install-target-libgcc
popd

# And that's it! You can delete everything if you like.
# rm -rf build binutils* gcc*
```

Getting Meson is much simpler, which is usually available through your system's
package manager.

# Building

The build is rather straight-forward. Just run the following in the root
directory of the project:

```sh
meson . build-x86 --cross-file=toolchains/x86.txt
cd build-x86
ninja
```
