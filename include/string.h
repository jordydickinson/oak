#ifndef OAK_STRING_H
#define OAK_STRING_H

#include <stddef.h>

void* memchr(const void *buf, int c, size_t n);
int memcmp(const void *lhs, const void *rhs, size_t n);
void* memcpy(void *dst, void *src, size_t n);
void* memset(void *dst, int c, size_t n);
size_t strlen(const char *str);

// TODO:
// memcpy_s
// memmove
// memmove_s
// memset_s
// strcat
// strcat_s
// strchr
// strcmp
// strcoll
// strcpy
// strcpy_s
// strcspn
// strerror
// strerror_s
// strerrorlen_s
// strncat
// strncat_s
// strncmp
// strncpy
// strncpy_s
// strnlen_s
// strpbrk
// strrchr
// strspn
// strstr
// strtok
// strtok_s
// strxfrm

#endif
