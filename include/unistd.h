#ifndef OAK_UNISTD_H
#define OAK_UNISTD_H

#include <stdint.h>

int brk(void *addr);
void* sbrk(intptr_t incr);

#endif
