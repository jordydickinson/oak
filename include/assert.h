#ifndef OAK_ASSERT_H
#define OAK_ASSERT_H

#ifdef NDEBUG
#define assert(COND) ((void)0)
#else
// TODO: Display a message on failure.
#define assert(COND) do {if (!COND) abort();} while(0)
#endif

#define static_assert _Static_assert

#endif
