#ifndef OAK_STDLIB_H
#define OAK_STDLIB_H

#include <stddef.h>
#include <stdnoreturn.h>

// Memory Management
void* malloc(size_t size);
void* calloc(size_t num, size_t size);
void* realloc(void *ptr, size_t size);
void free(void *ptr);
void* aligned_alloc(size_t alignment, size_t size);

// TODO: Program Support
_Noreturn void abort(void);
// exit
// quick_exit
// _Exit
// atexit
// at_quick_exit
// EXIT_SUCCESS
// EXIT_FAILURE
// system
// getenv
// getenv_s

// TODO: String Conversions (Byte)
// atof
// atoi
// atol
// atoll
// strtol
// strtoll
// strtoul
// strtoull
// strtof
// strtod
// strtold
// strtoimax
// strtoumax

// TODO: String Conversions (Multibyte/Wide)
// mblen
// mbtowc
// wctomb
// wctomb_s
// mbstowcs
// mbstowcs_s
// wcstombs
// wcstombs_s
// MB_CUR_MAX

// TODO: Pseudo-random Number Generator
// rand
// srand
// RAND_MAX

#endif
